---
title: "Service mesh"
date: 2020-01-25T16:58:43+01:00
draft: false

tags: ['IT', 'Software']

---

### Goal of this web site

I'm currently running consul as service mesh server for the control plane in the test environment.

Consul agents use Envoy as side car proxy.

I'm connecting my home network with an AWS account VPC.

All information and findings will be published here.

